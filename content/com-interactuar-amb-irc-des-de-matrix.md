# Com interactuar amb IRC des de Matrix

## Introducció
Una de les principals característiques de [Matrix][matrix] és que permet
interconnectar diferents protocols (no només de missatgeria instantània o IM)
en un mateix punt. Això ens permet, per exemple, usar una única aplicació (un
client de Matrix, que pot ser des d'[Element][element] que és el més conegut,
fins a qualsevol de les seves [alternatives][clients] com [nheko Reborn][nheko]
(escriptori), [FluffyChat][fluffychat] (mòbil) o [Fractal][fractal]
(escriptori) per anomenar-ne uns quants).

Aquesta interacció es coneix com a [_Bridges_][bridges] i és el que li dóna
valor afegit. No es tracta només d'una integració entre un protocol de
missatgeria i Matrix sinó a crear un teixit de diferents protocols que permeten
comunicar-se entre ells, creant una *matriu*. Exacte, el nom no té res a veure
amb la pel·lícula.

Quin és el principal avantatge que es desprèn d'això? Que només es necessita
una única aplicació i no una per a cada protocol.

De *bridges* n'hi ha principalment de dos tipus segons la classificació que
m'acabo d'inventar: unidireccionals i multidireccionals.

Els unidireccionals permeten recollir informació de l'exterior i mostrar-la a
la sala de Matrix que s'hagi configurat, i un exemple podria ser el de RSS
(el *bridge* va comprovant un canal RSS i quan té contingut nou el mostra com
un missatge).

Els multidireccionals permeten una interacció amb l'exterior i poden ser el
d'Slack, Whatsapp, correu electrònic o IRC. I d'aquest últim serà el que en
parlaré a continuació.

## Per què IRC?
Per què parlar de la integració amb IRC quan hi ha *bridges* amb
tecnologies/protocols més noves com Signal, Telegram o XMPP?

Doncs principalment per quatre motius:
- IRC és un protocol estàndard que porta molt temps funcionant
(sí, encara s'usa) i al llarg d'aquest temps ha demostrat **estabilitat** i
**compatibilitat**.
- És un protocol **obert**.
- Està basat exclusivament en **text** pla.
- Ens agradi o no, juntament amb les llistes de correu és la principal forma de
comunicació en equips de desenvolupament de programari lliure com per exemple
Debian.
- I perquè de totes les integracions (*bridges*) existents a Matrix, no totes
elles estan instal·lades a la instància de matrix.org (la principal en nombre
d'usuaris). I IRC ho està, de manera que virtualment tothom la podria usar. I
és que els *bridges* s'instal·len al servidor de Matrix, no als clients així
que tenir-los depèn de qui administri la instància.

## Som-hi
[Aquesta és part de la documentació oficial (anglès)][docu] per si li vols
donar un cop d'ull.

El que necessitem és:
1. Un compte d'usuari a una instància de Matrix. T'hauràs d'assegurar que la
instància tingui el *bridge* d'IRC instal·lat: [matrix.org][matrix] el té segur.
2. Un compte d'usuari a un servidor IRC. Les principals xarxes són [OFTC][oftc]
(és on hi ha els canals de Debian) i [Freenode][freenode] (és on hi eren abans),
així que et recomano un compte a cadascun d'ells.
3. Confiança. I és que sí, per tal que la integració de les dues xarxes
funcioni, hauràs de donar la contrasenya del teu usuari d'IRC (o usuaris, un
per a cada xarxa) a la instància de Matrix. És millor deixar-ho clar des d'un
principi en comptes de fer-ho sense ser-ne conscient.
4. Qualsevol client de Matrix.
5. Tenir present [aquesta taula][appservice-irc] on hi ha la relació entre el
format de sales, l'usuari a qui enviar comandes i cada xarxa d'IRC.

Un cop ho tinguem tot…
1. Iniciem una conversa amb el bot `@appservice-irc:matrix.org`; pots
escriure-li `!help` per veure totes les comandes disponibles.
2. Digues-li quin és el teu usuari d'IRC per a cadascuna de les xarxes:
`!nick irc.example.net usuari`.
3. Com que la majoria de canals requereixen autenticació per prevenir spam,
identifica't amb les teves credencials d'IRC. Per exemple, per a OFTC escriu-li
al bot `@_oftc_NickServ:matrix.org` el missatge `IDENTIFY contrasenya usuari`.
En canvi, per a Freenode, escriu-li al bot `@freenode_NickServ:matrix.org` el
missatge `identify contrasenya`.
4. Com que de vegades (cada cop menys) el _bridge_ s'ha de reiniciar i quan
això passa es perd l'autenticació, si deses la contrasenya el procés
d'autenticació es farà automàticament. Escriu-li al bot
`@appservice-irc:matrix.org` el missatge
`!storepass irc.example.net contrasenya`.

Ja ho tenim!

## Com unir-te a canals IRC de Debian
Independentment de si són públics o no,
[alguns dels canals **visibles** de Debian estan llistats aquí][debian-irc].
T'hi pots unir de qualsevol de les maneres següents:
- A Element, a la columna esquerra hi ha una icona d'una **brúixola** tant al
costat del camp de filtrar canals com quan prems el botó `+` de la secció de
 **sales**  Allà s'obre un cercador de sales públiques que pots filtrar per
sales OFTC (just a sota del quadre de cerca) i allà hi trobaràs sales com per
exemple `#debian-catalan`.
- Escriu-li al bot `@_oftc_NickServ:matrix.org` el missatge
`!join #debian-catalan`.
- Escriu en alguna sala (per exemple, crea'n una de privada per no fer soroll)
el text `#_oftc_#debian-catalan:matrix.org` i clica sobre l'enllaç.

## Resultat
D'aquesta només et caldrà una única aplicació (qualsevol client de Matrix!) per
interactuar tant amb la
[sala de DebianCat a Matrix `#debian-cat:matrix.org`][debiancat-irc] com amb el
canal d'IRC `#debian-catalan`.

[matrix]: https://www.matrix.org
[element]: https://element.io
[clients]: https://www.matrix.org/clients
[nheko]: https://github.com/Nheko-Reborn/nheko
[fluffychat]: https://fluffychat.im
[fractal]: https://www.matrix.org/docs/projects/client/fractal
[bridges]: https://matrix.org/bridges/
[docu]: https://github.com/matrix-org/matrix-appservice-irc/wiki/End-user-FAQ
[oftc]: https://www.oftc.net/
[freenode]: http://freenode.net/
[appservice-irc]: https://github.com/matrix-org/matrix-appservice-irc/wiki/Bridged-IRC-networks
[debian-irc]: https://wiki.debian.org/IRC#Debian_IRC_channels
[debiancat-irc]: https://matrix.to/#/#debian-cat:matrix.org
